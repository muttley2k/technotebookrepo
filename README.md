# Introduction
blabla
# Launch with Binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/muttley2k%2Ftechnotebookrepo/HEAD?urlpath=/notebooks/index.ipynb)

# Run locally on your computer
1. Clone this repository: `git clone https://gitlab.com/muttley2k/technotebookrepo.git`
2. Install the packages in 'requirements.txt': `pip install <package name>`
3. Install Jupyter Notebooks: `pip install notebook`
4. Start notebook server: `jupyter notebook`
5. In browser, select `index.ipynb` to open
# Open in Google Colab
1. Go to [GoogleColab](https://colab.research.google.com/), and in File menu, select **'New notebook'**
2. If not yet done, **clone this repository** in your Google Drive. To do that, run the following commands:
> `from google.colab import drive`
>
> `drive.mount('/content/drive')`
>
> `%cd /content/drive/My Drive/`
>
> `!git clone https://gitlab.com/muttley2k/technotebookrepo.git`
3. If needed, make the cloned repository **up-to-date with GitLab**. To do that, run the following commands:
> `from google.colab import drive`
>
> `drive.mount('/content/drive')`
>
> `%cd /content/drive/My Drive/technotebookrepo`
>
> `!git reset --hard`
>
> `!git pull`
4. In File menu, select **'Locate in Drive'**, and navigate to the notebook you wish to open
